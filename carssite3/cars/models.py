from django.db import models

class Car(models.Model):
    title = models.CharField(max_length=200, verbose_name='Заголовок')
    text = models.TextField(verbose_name='Описание машины')
    image = models.ImageField(verbose_name= 'Изображение машины',upload_to='cars_logo',blank=True,null=True)


def __str__(self):
    return self.title


