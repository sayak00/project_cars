from django.shortcuts import render,redirect,get_object_or_404
from .models import Car
from .form import CarForm


def car_list(request):
    cars = Car.objects.all()
    return render(request, 'car/car_list.html', {'cars':cars})


def car_create(request):
    if request.method == "POST":
        form = CarForm(request.POST,request.FILES)
        if form.is_valid():
            car = form.save(commit=False)
            car.save()
            return redirect('car_list')
    else:
        form = CarForm()
    return render(request, 'car/cars_create.html', {'form': form})


def car_update(request, pk):
    car = get_object_or_404(Car, pk=pk)
    if request.method == "POST":
        form = CarForm(request.POST,request.FILES,instance=car)
        if form.is_valid():
            car = form.save(commit=False)
            car.save()
            return redirect('car_list')
    else:
        form = CarForm(instance=car)
    return render(request, 'car/car_update.html', {'form': form})


def car_delete(request, pk):
    car = get_object_or_404(Car, pk=pk)
    car.delete()
    return redirect('car_list')

