from django.urls import path
from . import views

urlpatterns = [
    path('', views.car_list, name='car_list'),
    path('create/',views.car_create,name='cars_create'),
    path('<int:pk>/update/',views.car_update,name='car_update'),
    path('<int:pk>/delete/',views.car_delete,name='car_delete'),
]